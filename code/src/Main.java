import algorithms.BiSector;

import java.util.function.BiFunction;

public class Main {
    public static void main(String[] args) {
        double input, result;
        BiSector biSector;

        /* for each algorithm, you can define a plugin,
         * which calculates the difference between the given candidate and the target.
         */
        BiFunction<Double, Double, Double> logb = (n, candidate) -> Math.pow(2, candidate) - n;
        input = 1024;

        // the plugin can be fed into a BiSector
        biSector = new BiSector(logb);

        //which then uses it to binary search the space (in this case the real number line).
        result = biSector.bisect(input);
        System.out.printf("Function = logb, Input = %.5f, Result = %.5f%n", input, result);


        BiFunction<Double, Double, Double> sqrt = (n, candidate) -> candidate * candidate - n;
        input = 17.31;

        biSector = new BiSector(sqrt);
        result = biSector.bisect(input);
        System.out.printf("Function = sqrt, Input = %.5f, Result = %.5f%n", input, result);

    }

}

