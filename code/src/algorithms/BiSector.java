package algorithms;

import java.util.function.BiFunction;

/**
 * Bisects any input, given a BiFunction f(n, candidate) -> difference to target.
 * Runs in Θ(log n) for f(n, candidate) -> candidate * candidate - n; (Square Root)
 * and Θ(n) for f(n, candidate) -> Math.pow(2, candidate) - n; (Binary Logarithm)
 */
public class BiSector {

    private BiFunction<Double, Double, Double> plugin;

    private static final double APPROX = 1/1_000_000.0;

    public BiSector(BiFunction<Double, Double, Double> plugin){
        this.plugin = plugin;
    }

    /**
     * Searches for, and returns the number closest to the target as defined in the given plugin BiFunction.
     * It's satisfied when it's closer than 1/1_000_000, so given a sufficiently large input, it will not terminate
     * due to limits in tail recursion of the JVM.
     */
    public double bisect(double number) {
        return bisect(number, 0, number);
    }

    private double bisect(double n, double low, double high) {
        double candidate = (low + high) / 2;

        double diff = plugin.apply(n, candidate);

        if (diff > APPROX){
            return bisect(n, low, candidate);
        }
        if (-diff > APPROX){
            return bisect(n, candidate, high);
        }
        return candidate;
    }
}