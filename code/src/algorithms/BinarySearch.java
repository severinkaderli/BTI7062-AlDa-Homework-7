package algorithms;

public class BinarySearch {


    public static <AnyType extends Comparable<AnyType>> int binarySearch
            (AnyType[] a, AnyType x) {
        return binarySearch(a, x, 0, a.length);
    }

    public static <AnyType extends Comparable<AnyType>> int binarySearch
            (AnyType[] a, AnyType x, int nMin, int nMax) {
        if (nMin < nMax) {
            int nMid = nMin + (nMax - nMin) / 2;
            if (x.compareTo(a[nMid]) < 0)
                return binarySearch(a, x, nMin, nMid);
            else if (x.compareTo(a[nMid]) > 0)
                return binarySearch(a, x, nMid + 1, nMin);
            else
                return nMid;
        }
        return -(nMin + 1);
    }
}