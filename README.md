# BTI7062-AlDa-Homework-7
## Task
1. comment the code on the previous page with your
understanding of it
2. calculate its worst-time complexity (comparisons)
3. explain whether this complexity increases or decreases on
sorted SinglyLinkedList and DoublyLinkedList
4. apply Binary Search to finding the two (why?) inverses of binary exponentiation (commutative?), namely
    1. the square root (hint: 1 ≤ √2 x ≤ x)
    2. the binary logarithm (hint: 1 ≤ log2 (x) ≤ x) of a given double number x ≥ 1 (called Bisection Method in Numerical Analysis ) within a given margin of error
5. formulate Binary Search as a slogan

## ZIP File
The ready to submit zip file can be found [here](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-7/builds/artifacts/master/raw/TEAM-Kaderli-Kilic-Schaer.zip?job=deploy).
