---
paersize: a4
geometry: margin=2cm
fontsize: 10pt
author: [Kaderli Severin, Kilic Alan, Schär Marius]
header-includes: |
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[CO,CE]{This is fancy}
    \fancyfoot[CO,CE]{So is this}
    \fancyfoot[LE,RO]{\thepage}

---

# How to build
A precompiled jar is available in the `code/bin` folder. But you can compile it yourself using the following commands:
```bash
# Compile Java source code
javac -sourcepath src -d build code/src/**/*.java

# Create manifest files
echo -e "Manifest-Version: 1.0\nMain-Class: Main\n" > build/manifest.txt

# Change to build directory and generate jar files
cd build
jar cvfm ../Application.jar manifest.txt **/*.class

```

# How to run
```bash
mv Application.jar.nosuspicious Application.jar
java -jar Application.jar
```
The weird name ensures delivery by email.

\newpage
# Subtask 1
```{.java}
/**
 * Binary searches the List<E> a for the given target
 * Between the positions left and right.
 * int comparison tells us whether we need to look left or right
 * or if we've found the target.
 */
int binarySearch(int left, int right, Integer target) {
    // calculates the position of the middle element in the search space.
    int mid = (left + right) / 2;
    // compares the target with the middle element
    // -1 if target < mid
    //  0 if target = mid
    //  1 if target > mid
    int comparison = Integer.compare(target, a.get(mid));
    // if the search space contains only a single element
    if (right == left) {
        // if the target <= mid, return the left element (same as mid)
        // otherwise the one to the right
        return (comparison <= 0) ? left : right + 1;
        // if the mid element matches the target or the left is the same as the mid element
    } else if (comparison == 0 || (comparison < 0 && left == mid)) {
        // return the middle element
        return mid;
        // if the mid element is bigger than the target
    } else if (comparison < 0) {
        // search for the element between the left and middle positions
        return binarySearch(left, mid - 1, target);
    } else {
        // search for the element between the middle and right positions
        return binarySearch(mid + 1, right, target);
    }
}
```

\newpage
# Subtask 2
For Binary search, $T_{A}(n) = T(\frac{n}{2}) + \mathcal{O}(1)$

We can apply the Master Theorem $T_{A}(n) = a \cdot T_{A}(\frac{n}{b}) + f(n)$ as such:
$$
\begin{aligned}
  a &= 1 \\
  b &= 2 \\
  \log_{b}(a) &= 1 \\
  f(n) &= n^{c} \cdot log_{k}(n)
\end{aligned}
$$

where $k = 0$ and $c = log_{b}(a)$. Thus finally
$$
T_{A}(n) = \mathcal{O}(n^{c} \cdot log_{k + 1}(n) = \mathcal{O}(log\;n)
$$

\newpage
# Subtask 3
With both a SinglyLinkedList (SLL) or DoublyLinkedList (DLL), the complexity increases.

This is because we can't just `a.get(mid)` anymore,
as SLLs and DLLs aren't random access data structures.

Where an array based data structure (such as Java's `ArrayList`) has $\mathcal{O}(1)$ access time, SLLs or DLLs have $\mathcal{O}(n)$ access times
because we need to traverse the structure from the previous left or middle element to find the next middle element.

# Subtask 4
For our application of binary search, see the code directory.

## Complexity of Binary Searching for square root
Because checking the "closeness" of a candidate to the target for square root involves a single multiplication, it's $f(n) = \Theta(1)$ for the node work.

$$
\begin{aligned}
  a &= 1 \\
  b &= 2 \\
  c &= 1 \\
  l &= 0 \\
  k &= 0 \\
  T_{A}(n) &= \Theta(\Theta(1) \cdot log\;n) \\
  T_{A}(n) &= \Theta(log\;n)
\end{aligned}
$$

## Complexity of Binary Searching for log 2
Because checking the "closeness" of a candidate to the target for $log_{b}$ involves $n$ multiplications ($2^{candidate} = target$), it's $f(n) = \Theta(n)$ for the node work.

$$
\begin{aligned}
  a &= 1 \\
  b &= 2 \\
  c &= 1 \\
  l &= 1 \\
  k &= 0 \\
  T_{A}(n) &= \Theta(\Theta(n)) \\
  T_{A}(n) &= \Theta(n)
\end{aligned}
$$

We end up with $\Theta(log\;n)$ for square root and $\Theta(n)$ for $log_{b}$.

# Subtask 5
Try now! New and Improved!

*Binary Search* - Efficiently sectioning search space for $\mathcal{O}(log\;n)$ operation!*$_{\text{since 1946}}$*
