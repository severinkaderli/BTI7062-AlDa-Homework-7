/**
 * Binary searches the List<E> a for the given target
 * Between the positions left and right.
 */
int binarySearch(int left, int right, E target){
  // calculates the position of the middle element in the search space.
  int mid = (left+right) / 2;
  // compares the target with the middle element
  // -1 if target < mid
  //  0 if target = mid
  //  1 if target > mid
  int comparison = comp.compare(target, a.get(mid));
  // if the search space contains only a single element
  if (right==left){
    // if the target <= mid, return the left element (same as mid)
    // otherwise the one to the right
    return (comparison <= 0) ? left : right + 1;
  // if the mid element matches the target or the left is the same as the mid element
  }else if(comparison == 0 || (comparison < 0 && left==mid)){
    // return the middle element
    return mid;
  // if the mid element is bigger than the target
  }else if(comparison < 0){
    // search for the element between the left and middle positions
    return binarySearch(left, mid - 1, target);
  }else{
    // search for the element between the middle and right positions
    return binarySearch(mid + 1, right, target);
  }
}
